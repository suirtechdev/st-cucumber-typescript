import {browser} from "protractor";
import {CalculateActions} from "../../businesslogic/actions/calculate/CalculateActions";
import {CalculatorPage} from "../../businesslogic/pages/CalculatorPage";
import {AssertHelper} from "../../businesslogic/actions/helpers/AssertHelper";

export = function () {

    this.Given(/^I am on Googles Calculator Page$/, function () {
        return browser.get("").then(function () {
            return browser.sleep(200);
        });
    });


    this.When(/^I add two numbers "([^"]*)" and "([^"]*)"$/, function (arg1, arg2) {
        return CalculateActions.add(arg1, arg2);
    });

    this.Then(/^I verify result is "([^"]*)"$/, function (results) {
        return AssertHelper.elementEqualsText(CalculatorPage.result(), results)
    });


    this.Then(/^I verify result is not "([^"]*)"$/, function (results) {
        return CalculatorPage.result().getText().then(function (text) {
            AssertHelper.stringNotEquals(text, results)
        })

    });
};