let clc = require('cli-color');
let Cucumber = require('cucumber');
let JsonFormatter = Cucumber.Listener.JsonFormatter();
let fs = require('fs');
let jsonOutDir = "json_output";

module.exports = function JsonOutputHook() {
    if (!fs.existsSync(jsonOutDir)) {
        fs.mkdirSync(jsonOutDir);
    }

    let featureName = "";

    JsonFormatter.handleAfterScenarioEvent = function (scenario, callback) {
        console.log(clc.blue(`writing json result for ${featureName}`));
        featureName = scenario.getPayloadItem("scenario").getFeature().getName();
        callback();
    };

    JsonFormatter.log = function (json) {
        let jsonFile = `${jsonOutDir}/${featureName}_result.json`;

        fs.writeFile(jsonFile, json, function (err) {
            if (err) {
                throw err;
            }
            console.log(clc.blue("result json @ : " + jsonFile));
        });
    };

    this.registerListener(JsonFormatter);
};