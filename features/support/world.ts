import {browser} from "protractor";

(function () {
    console.log("World TS Loaded");

})();

export let World = {
    defaultTimeOut: 6000,
    inspect: require('util').inspect,
    faker: require('faker'),
    expect: require('chai').use(require('chai-as-promised')).expect,
    co: require('co'),
    clc: require('cli-color'),
    pause: function (secs: number, info: string = 'no pause info'): any {
        console.log(info);
        return browser.sleep(secs * 1000);
    }
};

