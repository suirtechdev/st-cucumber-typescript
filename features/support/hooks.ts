import { browser } from 'protractor';

import { LFService} from "typescript-logging"
import {World} from "./world";
import {Results} from "./results";
import {HttpHelper} from "../../businesslogic/actions/helpers/HttpHelper";

const factory = LFService.createLoggerFactory();
const logger = factory.getLogger("CukeHooks");

export = function () {


    this.After((scenario, done) => {
        logger.info(World.clc.red(`:::: COMPLETED AND PASSED: ${scenario.isSuccessful()}`));

        console.log("after hook --->" );
        const githubUrl = 'http://tomcat:8080/userservice/saveResult';




        if (scenario.isFailed()) {
            return browser.takeScreenshot().then(function (base64png) {
                let decodedImage = new Buffer(base64png, 'base64').toString('binary');
                scenario.attach(decodedImage, 'image/png');
                HttpHelper.post(githubUrl,Results.getBody("fail"), {} );
            }, (err) => {
                done(err);
            });
        } else {
            HttpHelper.post(githubUrl,Results.getBody("pass"), {} );
            done();
        }


    });


}