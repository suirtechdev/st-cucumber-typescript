export class Results {

    static getBody(result) {
        return {
            "name": "typescript-test",
            "type": "webdriver",
            "result": "" + result + "",
            "job_number": "" + process.env.JOB_NUMBER + "",
            "job_name": "" + process.env.JOB_NAME + "",
            "run_number": "" + process.env.RUN_NUMBER + "",
            "scenario_id": "111-1",
            "feature": "111"
        }
    }

}