Feature: Calculate Add Feature

  Background: Demo Typescript and Cucumber

  @Feat_1
  Scenario Outline: Add Two Numbers
    Given I am on Googles Calculator Page
    When I add two numbers "<number1>" and "<number2>"
    Then I verify result is "<result>"


    Examples:
      | number1 | number2 | result |
      | 1       | 2       | 3      |
      | 10      | 2       | 12     |
      | 10      | 3       | 12     |
      | 10      | 5       | 15     |
      | 10      | 4       | 14     |


  Scenario Outline: Add Two Numbers False
    Given I am on Googles Calculator Page
    When I add two numbers "<number1>" and "<number2>"
    Then I verify result is not "<result>"


    Examples:
      | number1 | number2 | result |
      | 1       | 2       | 4      |
      | 10      | 20      | 14     |
      | 10      | 20      | 14     |
      | 10      | 20      | 14     |
