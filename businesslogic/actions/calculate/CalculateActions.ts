import {CalculatorPage} from "../../pages/CalculatorPage";
import {World} from "../../../features/support/world";
import {LFService} from "typescript-logging";
import {browser} from "protractor";
import {WebdriverWebElement} from "protractor/built/element";
import {WElementFinder} from "../../../framework/WElementFinder";


export class CalculateActions {
    static factory = LFService.createLoggerFactory();
    static logger = CalculateActions.factory.getLogger(`ST ${CalculateActions.name}`);

    static add(number1: string, number2: string): void {
        CalculateActions.logger.info(`adding ${number1} & ${number2}`);
        return World.co(function* () {
            yield CalculateActions.selectNumber(number1);
            yield CalculatorPage.btnAdd().click();
            yield CalculateActions.selectNumber(number2);
            yield CalculatorPage.btnEquals().click();
        })

    }

    static selectNumber(number: string) {
        return World.co(function* () {
            for (let char of [...number]) {
                console.log("loop click :: " + char);
                yield CalculateActions.clickNumber(char);
            }
        })
    }

    static clickNumber(char: string) {
        console.log("will click " + char);
        let number: WElementFinder = null;
        switch (char) {
            case '0':
                number = CalculatorPage.number0();
                break;
            case '1':
                number = CalculatorPage.number1();
                break;
            case '2':
                number = CalculatorPage.number2();
                break;
            case '3':
                number = CalculatorPage.number3();
                break;
            case '4':
                number = CalculatorPage.number4();
                break;
            case '5':
                number = CalculatorPage.number5();
                break;
            case '6':
                number = CalculatorPage.number6();
                break;
            case '7':
                number = CalculatorPage.number7();
                break;
            case '8':
                number = CalculatorPage.number8();
                break;
            case '9':
                number = CalculatorPage.number9();
                break;
            default:
                console.log("wrong number passed");

        }

        return number.click();
    }

}