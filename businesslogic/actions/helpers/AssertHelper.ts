import {World} from "../../../features/support/world";
import {LFService} from "typescript-logging";
import {WElementFinder} from '../../../framework/WElementFinder'
import {promise as wdpromise} from 'selenium-webdriver'
import {strictEqual} from "assert";

let expect = World.expect;
let co = World.co;
let clc = World.clc;

let xtermRed = 8;

export class AssertHelper {

    static factory = LFService.createLoggerFactory();
    static logger = AssertHelper.factory.getLogger(`ST ${AssertHelper.name}`);

    /**
     * equalsText
     * @param {WElementFinder} actualElement element page object
     * @param {string} expected string expected
     *
     * @returns {any} return promise
     */
    static elementEqualsText(actualElement: WElementFinder, expected: string) {
        return co(function*() {
            let actualText = yield actualElement.getText();
            return AssertHelper.stringEquals(actualText, expected);
        })
    }


    /**
     * notEqualsText
     * @param {WElementFinder} actualElement element page object
     * @param {string} expected string expected
     *
     * @returns {any} return promise
     */
    static elementNotEqualsText(actualElement: WElementFinder, expected: string) {
        let selfObject = this;
        return co(function*() {
            let actualText = yield actualElement.getText();
            return AssertHelper.elementNotEqualsText(actualText, expected);
        })
    }

    /**
     * containsText
     * @param {WElementFinder} actualElement element page object
     * @param {string} expected string expected
     *
     * @returns {any} return promise
     */
    static elementContainsText(actualElement: WElementFinder, expected: string) {
        let selfObject = this;
        return co(function*() {
            let actualText = yield actualElement.getText();
            let logColour = clc.xterm(xtermRed);
            selfObject.logger.info(logColour(`expect text ${actualText} to contain ${expected}`));
            return expect(actualText).to.contain(expected);
        })
    }

    static stringEquals(actualString: String, expectedString: String){
        let logColour = clc.xterm(xtermRed);
        this.logger.info(logColour(`expect ${actualString} === ${expectedString}`));
        return expect(actualString).to.equal(expectedString);
    }

    static stringNotEquals(actualString: String, expectedString: String){
        let logColour = clc.xterm(xtermRed);
        this.logger.info(logColour(`expect ${actualString} != ${expectedString}`));
        return expect(actualString).to.not.equal(expectedString);
    }

}