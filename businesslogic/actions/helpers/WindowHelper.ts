import {browser} from "protractor";
import {World} from "../../../features/support/world";
import {LFService} from "typescript-logging";
import {promise as wdpromise} from 'selenium-webdriver'

let logColour = World.clc.xterm(0).bgXterm(120);

export class BrowserHelper {

    static factory = LFService.createLoggerFactory();
    static logger = BrowserHelper.factory.getLogger(`ST ${BrowserHelper.name}`);


    /**
     * SwitchWindow
     *
     *
     * @param index
     * @param page
     * @param isAngularPage | true if expecting site to be angular : returning to FR site
     * @returns {!webdriver.promise.Promise.<void>} A promise that will be resolved
     *     when the click command has completed.
     */
    static switchWindow(index: number, page: string, isAngularPage: boolean): wdpromise.Promise<void> {
        let thisObject = this;
        return World.co(function*() {
            let handles = yield browser.getAllWindowHandles();
            if(!isAngularPage) {
                thisObject.logger.info(logColour(`Switching to Non Angular Window ${page} @ Index ${index} `));
                thisObject.logger.info(logColour(`Ignore Synchronization ::: ${!isAngularPage}  `));
                browser.ignoreSynchronization = true;
            } else {
                thisObject.logger.info(logColour(`Switching to Non Angular Window ${page} @ Index ${index} `));
                thisObject.logger.info(logColour(`Ignore Synchronization ::: ${!isAngularPage}  `));
                browser.ignoreSynchronization = false;
            }
            yield browser.switchTo().window(handles[index]);
        })
    }

}

