import request = require("request");
import {World} from "../../../features/support/world";
import {LFService} from "typescript-logging";

let logColorGreen = World.clc.yellow.bgXterm(22);
let logColorRed = World.clc.green.red;
let sharedHeaders = {'content-type': 'application/json'};

export class HttpHelper {

    static factory = LFService.createLoggerFactory();
    static logger = HttpHelper.factory.getLogger(`FR ${HttpHelper.name}`);


    /**
     * HTTP GET Wrapper
     *
     * @param url
     * @returns {Promise<T>|Promise}
     */
    static get(url: string, headers: {} = sharedHeaders) {
        this.logger.info(logColorGreen(`GET : ${url} `));
        return new Promise((resolve, reject) => {
            return request.get({
                    url: url,
                    headers: headers,
                },
                (error, response, body) => {
                    if (!error) {
                            this.logger.info(logColorGreen("RESPONSE : " + body + " " + response.statusCode));
                            resolve(body);
                    } else {
                        this.logger.info(logColorRed(`GET ERROR! ::: ${error}`));
                        reject(error);
                    }
                });
        });

    }

    static post(url: string, body: any, headers: {} = sharedHeaders) {
        let selfObject = this;
        return new Promise(function (resolve, reject) {
            let jsonBody = JSON.stringify(body);
            selfObject.logger.info(logColorGreen(`POST ${url} body \n${jsonBody}`));
            request.post({
                    url: url,
                    headers: headers,
                    body: jsonBody
                },
                function (error, response, body) {
                    if (!error) {
                        resolve(body);
                        selfObject.logger.info(logColorGreen(`REPSONSE CODE ::: ${response.statusCode}`));
                    } else {
                        reject(error);
                    }
                });
        });
    }
}