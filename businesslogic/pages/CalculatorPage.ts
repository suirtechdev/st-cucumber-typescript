import {WElementFinder} from '../../framework/WElementFinder';

export class CalculatorPage {

    static number0(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number1", "#cwbt43");
    }

    static number1(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number1", "#cwbt33");
    }

    static number2(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number2", "#cwbt34");
    }

    static number3(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number1", "#cwbt35");
    }

    static number4(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number1", "#cwbt23");
    }

    static number5(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number2", "#cwbt24");
    }

    static number6(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number1", "#cwbt25");
    }

    static number7(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number1", "#cwbt13");
    }

    static number8(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number2", "#cwbt14");
    }

    static number9(): WElementFinder {
        return new WElementFinder(CalculatorPage, "number2", "#cwbt15");
    }

    static btnAdd(): WElementFinder {
        return new WElementFinder(CalculatorPage, "btnAdd", "#cwbt46");
    }

    static btnMulitply(): WElementFinder {
        return new WElementFinder(CalculatorPage, "btnAdd", "#cwbt26");
    }

    static btnEquals(): WElementFinder {
        return new WElementFinder(CalculatorPage, "btnEquals", "#cwbt45");
    }


    static result(): WElementFinder {
        return new WElementFinder(CalculatorPage, "result", "#cwtltblr > div.cwtlotc");
    }

}
