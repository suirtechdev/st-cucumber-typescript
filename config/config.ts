import { browser } from 'protractor';

export let config = {

    // directConnect: true,

    seleniumAddress: 'http://192.168.0.129:4444/wd/hub',
    // seleniumAddress: 'http://docker_hub_1:4444/wd/hub',
    directConnect: false,

    baseUrl: 'https://www.google.ie/search?q=calculator+google&oq=calculator&gs_l=psy-ab.3.0.0i71k1l4.0.0.0.10685.0.0.0.0.0.0.0.0..0.0....0...1..64.psy-ab..0.0.0.nsysOCuSocM/',

   // webDriverProxy: 'http://127.0.0.1:8888',
    includeStackTrace: true,


    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: [ "--headless", "--disable-gpu", "--window-size=800,600" ]
        },

        shardTestFiles: false,
        acceptSslCerts: true,
        maxInstances: 10

        // browserName: 'phantomjs',
        // version: '',
        // platform: 'ANY'
    },

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    specs: [
        '../../features/**/*.feature'
    ],

    onPrepare: () => {
        // browser.manage().window().maximize();
        browser.manage().timeouts().pageLoadTimeout(40000);
        browser.manage().timeouts().implicitlyWait(25000);
        browser.ignoreSynchronization = true;
    },
    cucumberOpts: {
        compiler: "ts:ts-node/register",
        strict: true,
        plugin: ["pretty"],
        require: ['../../features/**/*.ts'],
    //    tags: ['@tag1','@tag2']
    },

    timeout: 500000,
    allScriptsTimeout: 60000,
};