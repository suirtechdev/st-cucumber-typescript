import {suite, test, timeout} from "mocha-typescript";
import {AssertHelper} from "../businesslogic/actions/helpers/AssertHelper";
import {World} from "../features/support/world";

/**
 *
 * example unit test with mocha - typescript
 *
 * npm unitTests
 *
 */

const defaultTestTimeOut = 10000;

@suite("mocha TestExamples")
class TestExample {


    @test("Testing AssertHelper stringEquals")
    @timeout(defaultTestTimeOut)
    testStringEquals() {
        return World.co(function*() {
            let testString = "abc";
            AssertHelper.stringEquals(testString, testString)
        });

    }

    @test("Testing AssertHelper stringEquals")
    @timeout(defaultTestTimeOut)
    testNotStringEquals() {
        return World.co(function*() {
            let testString = "abc";
            let testString2 = "abcd";
            AssertHelper.stringNotEquals(testString, testString2)
        });

    }
}