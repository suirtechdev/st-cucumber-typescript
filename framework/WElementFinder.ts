/**
 * @file ElementFinder
 *
 * Wrapper Extension of protractors element ( ElementFinder )
 *
 * @author Shane Connolly |  05/01/2017
 *
 */

import {element, browser, by, ElementFinder, protractor, ElementArrayFinder} from 'protractor';
import {LFService} from "typescript-logging";
import {promise as wdpromise} from 'selenium-webdriver'
import {World} from "../features/support/world";

let colSearch = World.clc.yellow;
let colAction = World.clc.xterm(208); // orange
let colComplete = World.clc.green.bold;
let colWarning = World.clc.xterm(9).bold;

let attAction: string = "ATTEMPTING ACTION";

export class WElementFinder {

    factory = LFService.createLoggerFactory();
    logger = this.factory.getLogger(`ST ${WElementFinder.name}`);

    page: any;
    name: string;
    cssLocator: string;
    wrappedElement: ElementFinder;

    /**
     * Create WElementFinder - ST Wrapper on  ElementFinder - element.(by.css('#id'))
     *
     * @constructor
     * @param {any} cls - calling page e.g. HomePage
     * @param {string} name - Name given to pageObject e.g. btnLogin
     * @param {string} cssLocator - String Value of the locator of the element.
     */
    constructor(cls: any, name: string, cssLocator: string) {
        this.page = cls.name;
        this.name = name;
        this.cssLocator = cssLocator;
        this.wrappedElement = this.find();
    }


    /**
     * Click on element
     *
     * @return {Promise} promise<void>
     */
    click(): wdpromise.Promise<void> {
        this.logger.info(colAction(`${attAction} CLICK`));
        return this.handElementError(this.wrappedElement.click());
    }

    /**
     * Type String to element
     *
     * @param {string} value to send
     * @return {Promise} promise
     */
    sendKeys(value: string): wdpromise.Promise<void> {
        this.logger.info(colAction(`${attAction} SEND_KEYS ${value}`));
        return this.handElementError(this.wrappedElement.sendKeys(value));
    }

    /**
     * Get Text of element
     *
     * @return {Promise} promise<String>
     */
    getText(): wdpromise.Promise<string> {
        this.logger.info(colAction(`${attAction} GET_TEXT`));
        console.log("recived text : " + this.wrappedElement.getText().then(function (text) {
            console.log("--> " + text);
        }));
        return this.handElementError(this.wrappedElement.getText());
    }

    /**
     * Clear  Text of element
     *
     * @return {Promise} promise<void>
     */
    clear(): wdpromise.Promise<void> {
        this.logger.info(colAction(`${attAction} CLEAR`));
        return this.handElementError(this.wrappedElement.clear());
    }

    /**
     * isPresent  check if element is present
     *
     * @return {Promise} promise<boolean>
     */
    isPresent(): wdpromise.Promise<boolean> {
        this.logger.info(colAction(`${attAction} isPresent?`));
        return this.handElementError(this.wrappedElement.isPresent());
    }


    isPresentWithTimeOut() {
        this.logger.info(colAction(`${attAction} isPresentWithTimeOut?`));
        return new Promise((resolve) => {
            return this.wrappedElement.isPresent().then((present) => {
                    if(present) {
                        resolve(present);
                    } else {
                       console.log("ELEMNET NOT FOUND CONTINUEING " + this.name)
                    }
            });
        });
    }

    /**
     * isPresent  check if element is present
     *
     * @return {Promise} promise<boolean>
     */
    isSelected(): wdpromise.Promise<boolean> {
        this.logger.info(colAction(`${attAction} isSelected?`));
        return this.handElementError(this.wrappedElement.isSelected());
    }

    getOuterHtml(): wdpromise.Promise<string> {
        this.logger.info(colAction(`${attAction} getOuterHtml?`));
        return this.handElementError(this.wrappedElement.getOuterHtml());
    }


    /**
     * preformActions
     *
     * @param sentKey
     * @returns {webdriver.promise.Promise<void>}
     */
    static preformActions(sentKey: string): wdpromise.Promise<void> {
        console.log("PERFOMING ACTION SENDKEY ::: " + sentKey);
        return browser.actions().sendKeys(sentKey).perform();
    }


    // Privates
    /**
     * find - wrapper on element ( ElementFinder )
     *  @return {element} webdriver element
     */
    private find(): ElementFinder {
        this.logString(colSearch.underline('FIND'));
        return element(by.css(this.cssLocator));
    }


    logString(method: string, keys: string = ""): void {
        this.logger.info(colSearch(`${method} [${this.page}] | ${this.name} | ${keys} ${this.cssLocator}`));
    }


    /**
     * handElementError - promise rejected when element not found
     *
     * // TODO catch it properly AND exit, throw error should do this
     * // TODO Make sure after hook is been called
     *
     * @param elementPromise
     * @param thisObject
     //  */
    private handElementError(elementPromise, thisObject = this) {
        return elementPromise.then(
            function success() {
                thisObject.logger.info(colComplete(`ELEMENT ${thisObject.name} ACCESSED OK`));
                return elementPromise;
            }, function fail(error) {
                throw error;
            });
    }

}