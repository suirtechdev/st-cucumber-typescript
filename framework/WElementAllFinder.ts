/**
 * @file FRElementAllFinder
 *
 * Wrapper Extension of protactors element ( ElementAFinder )
 *
 * @author Shane Connolly |  05/01/2017
 *
 */

import {element, by, ElementFinder} from 'protractor';
import {LFService} from "typescript-logging";
import {World} from "../features/support/world";
import {WElementFinder} from "./WElementFinder";

let colSearch = World.clc.yellow;

export class WElementAllFinder extends WElementFinder {

    factory = LFService.createLoggerFactory();
    logger = this.factory.getLogger(`ST ${WElementAllFinder.name}`);

    wrappedElement: ElementFinder;

    /**
     * Create FRElementFinder - FR Wrapper on  ElementFinder - element.(by.css('#id'))
     *
     * @constructor
     * @param {any} cls - calling page e.g. HomePage
     * @param {string} name - Name given to pageObject e.g. btnLogin
     * @param {string} cssLocator - String Value of the locator of the element.
     * @param index
     */
    constructor(cls: any, name: string, cssLocator: string, index: number) {
        super(cls, name, cssLocator);
        this.wrappedElement = this.findByAll(index);
    }

    /**
     * find - wrapper on element ( ElementFinder )
     *  @return {element} webdriver element
     */
    findByAll(index: number): ElementFinder {
        super.logString(colSearch.underline('FIND'));
        return element.all(by.css(this.cssLocator)).get(index);
    }


}